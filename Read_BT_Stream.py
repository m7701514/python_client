from ctypes import sizeof
import socket
import struct
from functools import partial

import numpy as np
import math
import csv

import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
import matplotlib.animation as animation

import libscrc

from datetime import datetime




sensorMACAddress = 'E0:E2:E6:70:86:7E'
aquisitionDuration = 10

#  BTHENUM\Dev_E0E2E670867E


port = 1


s = socket.socket(socket.AF_BLUETOOTH, socket.SOCK_STREAM, socket.BTPROTO_RFCOMM)
s.connect((sensorMACAddress, port))
s.settimeout(1)  #non-blocking after one second
s.send(bytes('<stop>', 'UTF-8'))  # Send command to BT client

s.send(bytes('<start>', 'UTF-8'))  # Send command to BT client

#data = s.recv(12)  #read bluetooth data

previousByte = -1
isReadingPacket = False
sampleSize = 40
btReadSize = 4096

counterBytePos = 2
millisBytePos = 5
accelBytePos = 8
magBytePos = 14
gyroBytePos = 20
forceBytePos = 26
crcBytePos = 38

#conversion for MPU9250 ( And MPU6886 ? )
conversion_factor_accel = 16 / 32768    #to g
conversion_factor_mag = (((( 170 - 128) * 0.5) / 128) + 1)  #to µT
conversion_factor_gyro = 2000 / 32768   #to rad/s

stream = bytearray(b'\xff\xfa')
startByte = bytearray(b'\xff\xfa')

sensorOutputCounter = ([])
sensorOutputMillis = ([])
sensorOutputAccel = ([])
sensorOutputMag = ([])
sensorOutputGyro = ([])
sensorOutputForce = ([])

counter = 0
millis = 0
millisOld = 0 
accel = [0, 0, 0]
accel_fix = [0, 0, 0] #needed to fix order due to bug in embedded code
mag = [0, 0, 0]
gyro = [0, 0, 0]
gyro_fix = [0, 0, 0] #needed to fix order due to bug in embedded code
accel_converted = [0.0, 0.0, 0.0]
mag_converted = [0.0, 0.0, 0.0]
gyro_converted = [0.0, 0.0, 0.0]
force = [0, 0, 0, 0, 0, 0]

crc_received = 0  #stores the crc calculated on the embedded device side
crc_calculated = 0  #stores the crc calculated on the PC side

buffer = bytearray(b'\x00')

chunk_size = 1
            
# unpack the sensor data from bytes to floats
def unpack_sensor_data(stream):
    
    # a[0:2] gives the first two elements
    #for x in range(2, 6):
        #print(x) 2,3,4,5  (and not 6!)

    #print(' '.join(format(x, '02x') for x in stream))
    global counter
    counterOld = counter
    counter =  int.from_bytes(stream[counterBytePos:counterBytePos+3], 'little') 

    if(counter - counterOld != 1):
        print(now, ": ", "Error: current Counter: ", counter, " previous counter: ", counterOld, " jump: ", int(counter - counterOld) )

    global millisOld
    
    millis =  int.from_bytes(stream[millisBytePos:millisBytePos+3], 'little')  

    

    # for i in range(0,3):
    #     accel[i] = float(int.from_bytes(stream[accelBytePos+i*2:accelBytePos+2+i*2], 'little')) 
    #     mag[i] = float(int.from_bytes(stream[magBytePos+i*2:magBytePos+2+i*2], 'little'))
    #     gyro[i] = float(int.from_bytes(stream[gyroBytePos+i*2:gyroBytePos+2+i*2], 'little'))

    accel = struct.unpack('hhh', stream[accelBytePos:accelBytePos+6]) 
    mag = struct.unpack('hhh', stream[magBytePos:magBytePos+6]) 
    gyro = struct.unpack('hhh', stream[gyroBytePos:gyroBytePos+6]) 
    
    #fix axis order for bug in FIFO readout 
    #TODO: remove / disable for normal readout modus on embedded system

    accel_fix[0] = int(gyro[1])
    accel_fix[1] = int(gyro[2])
    accel_fix[2] = int(accel[0])
    
    gyro_fix[0] = int(accel[1])
    gyro_fix[1] = int(accel[2])
    gyro_fix[2] = int(gyro[0])

    # accel[0] = accel_fix[0]
    # accel[1] = accel_fix[1]
    # accel[2] = accel_fix[2]

    # gyro[0] = gyro_fix[0]
    # gyro[1] = gyro_fix[1]
    # gyro[2] = gyro_fix[2]


    #convert int to real units 
    for i in range(0,3):
        accel_converted[i] = float(accel_fix[i]) * conversion_factor_accel
        mag_converted[i] = float(mag[i]) * conversion_factor_mag
        gyro_converted[i] = float(gyro_fix[i]) * conversion_factor_gyro



    
    force = struct.unpack('hhhhhh', stream[forceBytePos:forceBytePos+12])
    

    if(millis - millisOld > 1000):
        millisOld = millis 
        print(now, ": counter", counter, ",  millis: ", millis, "accel_x: ", accel_converted[0], "force: ", force[0]," | ", force[1]," | ", force[2]," | ", force[3]," | ", force[4]," | ", force[5])

    # Create lists with the sensor data
    sensorOutputCounter.append(counter)
    sensorOutputMillis.append(millis)
    sensorOutputAccel.append(accel_converted)
    sensorOutputMag.append(mag_converted)
    sensorOutputGyro.append(gyro_converted)
    sensorOutputForce.append(force)
    
    
    

now = datetime.today().timestamp()  # Get timezone naive now
startTime = int( now )
currentTime = int(now)

position = 0 #seek position through the raw data array
newPosition = 0 

while currentTime - startTime < aquisitionDuration:
    now = datetime.today().timestamp()  # Get timezone naive now
    currentTime = int(now)
    data = s.recv(btReadSize) #read bluetooth data

    now = datetime.now().time() # time object
    
    if data:
        buffer += data
        # print(now, ": ", len(buffer)/40)
        
    else:
        print(now, ": ", "no data read, stopping")
        s.connect((sensorMACAddress, port))

        s.send(bytes('<stop>', 'UTF-8'))  # Send command to BT client
        s.close()
        break
    

    #buffer.seek(0, 2)  # Seek the end
    num_bytes = len(buffer)  # Get the buffer size

    #from example extract_pngs.py    (https://www.devdungeon.com/content/working-binary-data-python)
    count = 0
  

    for i in range(0, math.ceil(btReadSize/sampleSize)):
        newPosition = buffer.find(startByte, position+1, num_bytes - sampleSize)
        if newPosition != -1:
            oneRawSample = buffer[position:position+sampleSize]

            #read crc provided by device
            crc_received = int.from_bytes(oneRawSample[crcBytePos:crcBytePos+2], 'little')
            
            #calculate crc 
            crc_calculated = libscrc.ccitt_false(oneRawSample[0:sampleSize-2])  #libscrc.ccitt_false uses 0xFFFF as init byte
            #compare calculated and received CRC
            
            if(crc_received == crc_calculated):
                unpack_sensor_data(oneRawSample) #convert raw bytes to decimal values of the individual channels
            #else:

                #print(now, ": ", "startPosition: ", newPosition, " crc received: ", crc_received, " crc calculated: ", crc_calculated )
            
            position = newPosition  #move the pointer such that the beginning of the buffer is not parsed again
            
            






s.send(bytes('<stop>', 'UTF-8'))  # Send command to BT client



# Writing the sensor data into a cvs file
with open('sensorData.csv', 'w') as csv_file:
    csv_writer = csv.writer(csv_file, delimiter=',')
    csv_writer.writerow(sensorOutputCounter)
    csv_writer.writerow(sensorOutputMillis)
    csv_writer.writerow(sensorOutputAccel)
    csv_writer.writerow(sensorOutputMag)
    csv_writer.writerow(sensorOutputGyro)
    csv_writer.writerow(sensorOutputForce)